package com.tcb.CountTheNumberOfTripForEachDirection;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class NumberOfTripsEachDirectionMapper extends Mapper<Object, Text, Text, IntWritable> {

	private BufferedReader reader;
	int number_missing_directions = 0;
	int nbr_missing = 0;

	public void map(Object key, Text value, Mapper<Object, Text, Text, IntWritable>.Context context)
			throws IOException, InterruptedException {
		
		
		

		ArrayList<String> list = new ArrayList<String>();
		reader = new BufferedReader(new FileReader("/home/nassereddine/Téléchargements/taxi+_zone_lookup.csv"));
		String str = "";

		String[] zoneId = null;
		String csvline = "";
		csvline = reader.readLine();
		while ((csvline = reader.readLine()) != null) {

			zoneId = csvline.trim().split(",");
			for (int i = 0; i < zoneId.length; i++) {
				list.add(zoneId[i]);
			}
		}
		String[] line = value.toString().split(",");
		if (value.toString() != null) {

			if (value.toString().contains(
					"TripID") /* Some condition satisfying it is header */)
				return;
			else {

				line = value.toString().split(",");

				String dropoff_taxizone_id = line[4];
				String pickup_taxizone_id = line[11];
				int dropoff_taxizone_id_index = list.indexOf(dropoff_taxizone_id);
				int pickup_taxizone_id_index = list.indexOf(pickup_taxizone_id);
				if (list.get(dropoff_taxizone_id_index + 1).contentEquals("1")
						|| (list.get(pickup_taxizone_id_index + 1).contentEquals("1"))) {
					str = "the number of missing directions is :";
					context.getCounter(COUNTERS.ERROR_COUNT).increment(1);
				} else {
					str = list.get(pickup_taxizone_id_index + 1) + "_ " + list.get(pickup_taxizone_id_index + 2)
							+ " ---> " + list.get(dropoff_taxizone_id_index + 1) + " _"
							+ list.get(dropoff_taxizone_id_index + 2);

				}
				Text outputKey = new Text(str);
				IntWritable outputValue = new IntWritable(1);
				context.write(outputKey, outputValue);
			}
			//

		}
	}
	public static enum COUNTERS {

		ERROR_COUNT,

		MISSING_FIELDS_RECORD_COUNT

		}

}
