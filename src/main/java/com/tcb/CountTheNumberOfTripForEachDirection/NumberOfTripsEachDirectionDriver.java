package com.tcb.CountTheNumberOfTripForEachDirection;

import java.io.IOException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.net.ntp.TimeStamp;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.Counters.Counter;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import com.tcb.CountTheNumberOfTripForEachDirection.NumberOfTripsEachDirectionMapper.COUNTERS;

public class NumberOfTripsEachDirectionDriver {
	public final static String INPUT_PATH = "hdfs://192.168.1.113:8020/user/ex-hadoop/yellow-taxi.csv";
	public final static String OUTPUT_PATH = "file:///home/nassereddine/workspace/CountTheNumberOfTripForEachDirection/target/";
	public final static String outputpath = "hdfs://192.168.1.113:8020/user/nacer/output";
	public static void main(String[] args)
			throws IllegalArgumentException, IOException, ClassNotFoundException, InterruptedException {

		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf);
		job.addCacheFile(new Path("hdfs://192.168.1.113:8020/user/ex-hadoop/").toUri());


		conf.addResource(new Path("/hadoop/projects/hadoop-2/conf/core-site.xml"));
		conf.addResource(new Path("/hadoop/projects/hadoop-2/conf/hdfs-site.xml"));

		FileInputFormat.setInputPaths(job, new Path(INPUT_PATH));
		//FileOutputFormat.setOutputPath(job, new Path(OUTPUT_PATH + TimeStamp.getCurrentTime()));
		FileOutputFormat.setOutputPath(job, new Path(outputpath));
		
		job.setJarByClass(NumberOfTripsEachDirectionDriver.class);
		job.setMapperClass(NumberOfTripsEachDirectionMapper.class);
        job.setReducerClass(NumberOfTripsEachDirectionReducer.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		

		System.exit(job.waitForCompletion(true) ? 0 : 1);
		Counter errorCounter = (Counter) job.getCounters().findCounter(COUNTERS.ERROR_COUNT);
		System.out.println("Number of Missing Directions = "+errorCounter.getValue());
//		URL url = new URL("hdfs://192.168.1.113:50070/user/ex-hadoop/yellow-taxi.csv");
//		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
//		con.setRequestMethod("POST");
//		con.setDoOutput(true);
//		con.setDoInput(true);
		//con.getOutputStream().write();;
		

	}
	

}
