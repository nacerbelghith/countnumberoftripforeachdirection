package com.tcb.CountTheNumberOfTripForEachDirection;

//Spublic class NumberOfTripsEachDirectionReducer {

	
	//package com.tcb.CountTripsFromParquetFile;

	import java.io.IOException;
	import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

	public class NumberOfTripsEachDirectionReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

		@Override
		public void reduce(Text key, Iterable<IntWritable> values,
				Reducer<Text, IntWritable, Text, IntWritable>.Context context)
				throws IOException, InterruptedException {

			int nbr = 0;
			for (IntWritable value : values) {
				nbr +=value.get();
			}

			context.write(key, new IntWritable(nbr));
		}

	}


